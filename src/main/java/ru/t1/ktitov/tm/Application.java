package ru.t1.ktitov.tm;

import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;

import java.util.Scanner;



public final class Application {

    public static void main(String[] args) {
        if(processArguments(args)) System.exit(0);
        processCommands();
    }

    private static void processCommand(final String command) {
        if(command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showErrorCommand();
        }
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter command: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArgument(final String argument) {
        if(argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private static void exit() {
        System.exit(0);
    }

    public static void showErrorArgument() {
        System.err.println("Error! Not supported argument");
        System.out.println();
    }

    public static void showErrorCommand() {
        System.err.println("Error! Not supported command");
        System.out.println();
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Titov Kirill");
        System.out.println("E-mail: kir.titoff@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show application version \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show application commands \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Exit \n", TerminalConst.EXIT);
    }

}
